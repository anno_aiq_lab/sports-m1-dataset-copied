# in/out utils

def txt2list(filepath):
    """
    read dataset information from txt file (e.g. train_partition.txt)
    and return it as list of dictionary.
    [{"url": ---.com, "classes": [12, 39]} ... ]
    :param filepath:
    :return:
    """
    datainfo = []
    with open(filepath) as f:
        for line in f.readlines():
            line = line.rstrip()
            line = line.split(" ")
            if len(line) < 2:
                continue
            url = line[0]
            classes_str = line[1].split(",")
            classes = [int(c) for c in classes_str]
            datainfo.append({"url": url, "classes":classes})
    return datainfo

