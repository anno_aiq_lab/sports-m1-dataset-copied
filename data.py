"""
Download youtube 1m dataset as image frame format.
"""
import argparse
import os
import glob
import youtube_dl
import cv2
from tqdm import tqdm

from io_utils import txt2list


def download(url, dir_name):
    """
    download a movie from given url and save it as .mkv format
    :param url:       youtube video url, str
    :param base_name: base name of movie to save, str
    :return:
    """
    if not os.path.exists(dir_name):
        os.makedirs(dir_name, exist_ok=True)
    options = {
        "outtmpl": "{VIDEO_DIR}/%(title)s".format(VIDEO_DIR=dir_name),
        "merge_output_format": "mkv",
        "format": "bestvideo[ext=mp4]+bestaudio",
        "quiet": True
    }
    with youtube_dl.YoutubeDL(options) as y:
        info = y.extract_info(url, download=True)
        print("Downloading {url} finish!".format(url=url))
    return info


def rename(info, dir_name, base_name, ext='tmp'):
    """
    Rename downloaded video filename as camelcase.
    :param info: download video information, dict
    """
    title = info["title"]
    pattern = '{VIDEO_DIR}/{title}*'.format(VIDEO_DIR=dir_name, title=title)
    for v in glob.glob(pattern, recursive=True):
        print("{title}.mp4 found! Renaming start..".format(title=title))
        new_file_path = '{VIDEO_DIR}/{title}.{ext}'.format(VIDEO_DIR=dir_name, title=base_name, ext=ext)
        os.rename(v, new_file_path)
        print("Renaming finish!".format(title))


def video2frame(dir_name, video_name, base_name):
    """
    Load video and save each frame to png format.
    :param dir_name:  video directory name, str
    :param base_name: video base name, str
    :return:
    """
    video_path = os.path.join(dir_name, video_name)
    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        return

    save_dir = os.path.join(dir_name, base_name)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    digit = len(str(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))))

    frame_idx = 0
    while True:
        ret, frame = cap.read()
        frame_idx += 1
        if ret:
            cv2.imwrite("{SAVE_DIR}/{IMG_NAME}.png".format(SAVE_DIR=save_dir, IMG_NAME=str(frame_idx).zfill(digit)),
                        frame)
        else:
            return


def prepare(txt_path, dir_name):
    """
    prepare full dataset
    :param txt_path:
    :param dir_name:
    :return:
    """
    datainfo = txt2list(txt_path)
    for d in tqdm(datainfo):
        try:
            save_dirname = d["url"].split("=")[-1]
            info = download(d["url"], dir_name)
            rename(info, dir_name, "sample", ext="tmp")
            video2frame(dir_name, "sample.tmp", save_dirname)
            os.remove(os.path.join(dir_name, "sample.tmp"))
        except:
            continue


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--txt_path', type=str, required=True)
    parser.add_argument('-d', '--dir_name', type=str, required=True)

    args = parser.parse_args()
    file_path = args.txt_path
    dir_name = args.dir_name

    prepare(file_path, dir_name)
