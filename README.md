# Sports 1m Dataset Downloader

## Usage 

Example usage for running.

```python
python data.py -p ${path/to/data.txt} -d ${path/to/save_dir}
```

For instance,

```python
python data.py -p original/train_partition.txt -d train_data
```

Then all data for training will be saved in `train_data` directory as following.

```
train_data
    - ABPsSSS2uY0 (movie hash)
        - 000.png
        - 001.png
        ...
    ...
```
